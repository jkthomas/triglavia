extends KinematicBody2D

var ENEMY_PUSHBACK_IMPACT = 1000

var enemy_velocity_vector = Vector2.ZERO

onready var soft_collision = $SoftCollision

func _physics_process(delta):
	if(soft_collision.is_colliding()):
		enemy_velocity_vector = soft_collision.get_push_vector() * delta * ENEMY_PUSHBACK_IMPACT
		enemy_velocity_vector = move_and_slide(enemy_velocity_vector)


func _on_EnemyHurtbox_area_entered(area):
	print(area)
	#queue_free()
