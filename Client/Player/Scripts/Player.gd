extends KinematicBody2D

var PLAYER_ACCELERATION = 300
var PLAYER_MAX_SPEED = 80
var PLAYER_FRICTION = 500
var PLAYER_PUSHBACK_IMPACT = 700
var player_velocity_vector = Vector2.ZERO

enum player_states {
	IDLE,
	MOVE,
	ATTACK
}
var player_state = player_states.MOVE

onready var soft_collision = $SoftCollision
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get('parameters/playback')

func _ready():
	animationTree.active = true

func _physics_process(delta):
	match player_state:
		player_states.MOVE:
			update_player(delta)
			player_velocity_vector = move_and_slide(player_velocity_vector)
			if player_velocity_vector != Vector2.ZERO:
				animationState.travel('walk')
			else:
				animationState.travel('idle')
		player_states.ATTACK:
			player_velocity_vector = Vector2.ZERO
			animationState.travel('attack')

func update_player(delta):
	var movement_vector = Vector2.ZERO
	movement_vector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	movement_vector.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	movement_vector = movement_vector.normalized()
	
	if movement_vector != Vector2.ZERO:
		animationTree.set("parameters/idle/blend_position", movement_vector)
		animationTree.set("parameters/walk/blend_position", movement_vector)
		animationTree.set("parameters/attack/blend_position", movement_vector)
		player_velocity_vector = player_velocity_vector.move_toward(movement_vector * PLAYER_MAX_SPEED, PLAYER_ACCELERATION * delta)
	else:
		player_velocity_vector = player_velocity_vector.move_toward(Vector2.ZERO, PLAYER_FRICTION * delta)
	
	if(soft_collision.is_colliding()):
		player_velocity_vector = soft_collision.get_push_vector() * delta * PLAYER_PUSHBACK_IMPACT

	if Input.is_action_pressed("attack"):
		player_state = player_states.ATTACK
			
func attack_animation_finished():
	player_state = player_states.MOVE
