extends Area2D

func is_colliding():
	var overlapping_areas = get_overlapping_areas()
	return overlapping_areas.size() > 0

func get_push_vector():
	var overlapping_areas = get_overlapping_areas()
	var soft_push_vector = Vector2.ZERO
	var area = overlapping_areas[0]
	soft_push_vector = area.global_position.direction_to(global_position)
	soft_push_vector = soft_push_vector.normalized()
	return soft_push_vector
